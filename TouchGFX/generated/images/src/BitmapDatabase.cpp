// 4.14.0 0xb9584415
// Generated by imageconverter. Please, do not edit!

#include <BitmapDatabase.hpp>
#include <touchgfx/Bitmap.hpp>

extern const unsigned char image_blue_backgrounds_main_bg_portrait_texture_240x320px[]; // BITMAP_BLUE_BACKGROUNDS_MAIN_BG_PORTRAIT_TEXTURE_240X320PX_ID = 0, Size: 240x320 pixels

const touchgfx::Bitmap::BitmapData bitmap_database[] =
{
    { image_blue_backgrounds_main_bg_portrait_texture_240x320px, 0, 240, 320, 0, 0, 240, (uint8_t)(touchgfx::Bitmap::RGB565) >> 3, 320, (uint8_t)(touchgfx::Bitmap::RGB565) & 0x7 }
};

namespace BitmapDatabase
{
const touchgfx::Bitmap::BitmapData* getInstance()
{
    return bitmap_database;
}

uint16_t getInstanceSize()
{
    return (uint16_t)(sizeof(bitmap_database) / sizeof(touchgfx::Bitmap::BitmapData));
}
}
